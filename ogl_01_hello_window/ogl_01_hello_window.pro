TARGET = hello_window

CONFIG += c++11

LIBS += -lglfw3 -lGL -lX11 -lXi -lXrandr -lXxf86vm -lXinerama -lXcursor -pthread -ldl

#INCLUDEPATH += GLFW/ glad/

SOURCES += \
    main.cpp \
    /usr/local/src/glad.c

#HEADERS +=
